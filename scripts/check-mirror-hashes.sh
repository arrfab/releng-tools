#!/bin/bash
#
# Check that the mirrorlist database has the correct hashes for centos stream repo_list
#
# Provide what CentOS Stream version we are checking
#     Example: 10

version="${1}"
version_list="9 10"
if ! [[ " ${version_list[*]} " =~ " ${version} " ]] ; then
    echo ""
    echo "ERROR: ${version} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${version_list[*]}"
    exit 1
fi

arch_list="aarch64 ppc64le s390x x86_64"
repo_list="baseos/BaseOS appstream/AppStream crb/CRB"

#For just once
arch="aarch64"
repo_low="appstream"
repo_high="AppStream"

starting_stage_hash=$(curl --connect-timeout 5 -s http://download.stream.rdu2.redhat.com/staged/${version}-stream/${repo_high}/${arch}/os/repodata/repomd.xml | sha512sum | awk '{print $1}')
starting_database_hash=$(curl --connect-timeout 5 -s "https://mirrors.fedoraproject.org/metalink?repo=centos-${repo_low}-${version}-stream&arch=${arch}" | grep sha512 | cut -d'>' -f2 | cut -d'<' -f1)

# Let's see how everything starts
echo "STARTING HASHES"
date
echo ${starting_stage_hash}
echo ${starting_database_hash}
echo

if [ "${starting_stage_hash}" == "${starting_database_hash}" ] ; then
    DONE="YES"
    echo "Hashes match.  Assuming database has been updated. Proceeding"
else
    DONE="NO"
    echo "Hashes do not match. Assuming database needs to be updated"
    echo "  $(date +%Y-%m-%d_%H:%M) waiting ..."
fi
while [ "${DONE}" == "NO" ]
do
    sleep 120
    now_database_hash=$(curl --connect-timeout 5 -s "https://mirrors.fedoraproject.org/metalink?repo=centos-${repo_low}-${version}-stream&arch=${arch}" | grep sha512 | cut -d'>' -f2 | cut -d'<' -f1)
    if [ "${starting_database_hash}" != "${now_database_hash}" ] ; then
            DONE="YES"
    else
        echo "  $(date +%Y-%m-%d_%H:%M) waiting ... ${now_database_hash}"
    fi
done

# Database is updated, check them all
TOTAL_STATUS="PASS"
echo
echo "WORKING ON ${version}-Stream"
for arch in $arch_list
do
    ARCH_STATUS="PASS"
    for repo in $repo_list
    do
        repo_low=$(echo ${repo} | cut -d'/' -f1)
        repo_high=$(echo ${repo} | cut -d'/' -f2)
        stage_hash=$(curl --connect-timeout 5 -s http://download.stream.rdu2.redhat.com/staged/${version}-stream/${repo_high}/${arch}/os/repodata/repomd.xml | sha512sum | awk '{print $1}')
        database_hash=$(curl --connect-timeout 5 -s "https://mirrors.fedoraproject.org/metalink?repo=centos-${repo_low}-${version}-stream&arch=${arch}" | grep sha512 | cut -d'>' -f2 | cut -d'<' -f1)
        if [ "${stage_hash}" == "${database_hash}" ] ; then
            echo "    PASS ${version}-Stream ${arch} ${repo_high}"
        else
            echo "    FAIL ${version}-Stream ${arch} ${repo_high}"
            echo "      ${stage_hash}"
            echo "      ${database_hash}"
            ARCH_STATUS="FAIL"
            TOTAL_STATUS="FAIL"
        fi
    done
done
if [ "${TOTAL_STATUS}" == "PASS" ] ; then
    echo
    echo "OVERALL: ${version}-Stream PASSED"
    echo
    exit 0
else
    echo
    echo "OVERALL: ${version}-Stream FAILED"
    echo
    exit 1
fi
