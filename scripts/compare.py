#!/usr/bin/python3

import koji

import argparse
import datetime
import jinja2
import json
import logging
import os
import re
import requests
import rpm
import sys

SCRIPTPATH = os.path.dirname(os.path.realpath(__file__))

class BuildSource:
    def __init__(
        self,
        source_id=None,
        infra=None,
        tag=None,
        product=None,
    ):
        """Setup a source of the builds

        :infra: Koji or Brew session,
        :tag: Koji or Brew tag
        """

        if source_id:
            infra, tag, product = self._configure_source(source_id)

        self.infra = infra
        self.tag = tag
        self.product = product
        self.cache = {}

        self.make_cache()

    def __str__(self):
        return f'{self.product}'

    def _configure_source(self, source_id):
        infra = koji.ClientSession('https://kojihub.stream.rdu2.redhat.com/kojihub')
        if source_id == "stream-gate":
            tag = "c9s-gate"
            product = "Stream9Gate"
        if source_id == "stream-pending":
            tag = "c9s-pending"
            product = "Stream9Pending"
        if source_id == "stream-candidate":
            tag = "c9s-candidate"
            product = "Stream9Candidate"
        if source_id == "stream10-gate":
            tag = "c10s-gate"
            product = "Stream10Gate"
        if source_id == "stream10-pending":
            tag = "c10s-pending"
            product = "Stream10Pending"
        if source_id == "stream10-candidate":
            tag = "c10s-candidate"
            product = "Stream10Candidate"

        return infra, tag, product

    def get_build(self, package):
        """Find the latest build of a package available in the build source

        Return None if there is no builds found
        """

        if self.cache:
            if package in self.cache:
                logging.debug(f'Read cached for {package} in {self}')
                return self.cache[package]
            else:
                logging.debug(f'Package {package} not found in cached {self}')
                return None

        builds = self.infra.listTagged(self.tag, package=package, latest=True)

        if builds:
            return builds[0]
        else:
            return None

    def make_cache(self):
        """Fetch all builds from tag"""

        logging.debug(f'Make cache for {self}...')
        builds = self.infra.listTagged(self.tag, latest=True)
        for build in builds:
            self.cache[build["name"]] = build
        logging.debug(f'Done making cache for {self}')


class Comparison:

    status = {
        -3: "S2ONLY",
        -2: "S1ONLY",
        -1: "NEW",
        0: "SAME",
        1: "OLD",
        2: "NONE",
    }

    def __init__(self, source1, source2):
        self.source1 = source1
        self.source2 = source2

        arches = ["aarch64", "ppc64le", "s390x", "x86_64"]

        self.results = {}

    def compare_one(self, package):
        """Return comparison data for a package

        Return dictionary with items: status, nvr1, nvr2.
        """
        if package in self.results:
            return self.results[package]

        build1 = self.source1.get_build(package)
        build2 = self.source2.get_build(package)

        if not build1:
            if not build2:
                logging.debug(f'Package {package} not found in {source1} or {source2}')
                return {
                    "status": self.status[2],
                    "nvr1": None,
                    "nvr2": None if not build2 else build2['nvr'],
                }
            else:
                logging.debug(f'Package {package} not found in {source1}')
                return {
                    "status": self.status[-3],
                    "nvr1": None,
                    "nvr2": None if not build2 else build2['nvr'],
                }

        if not build2:
            logging.debug(f'Package {package} not found in {source2}')
            return {
                "status": self.status[-2],
                "nvr1": build1["nvr"],
                "nvr2": None,
            }

        return {
                "status": self.status[compare_builds(build1, build2)],
                "nvr1": build1["nvr"],
                "nvr2": build2["nvr"],
        }

    def compare_content(self):
        pkglist = []
        for newpkg, build in self.source1.cache.items():
            if newpkg not in pkglist:
                pkglist.append(newpkg)
        for newpkg, build in self.source2.cache.items():
            if newpkg not in pkglist:
                pkglist.append(newpkg)
        #print(pkglist)
        for package in sorted(pkglist):
            logging.debug(f'Processing package {package}')
            self.results[package] = self.compare_one(package)
        return self.results

    def count(self):
        stats = {}
        for item in self.results.values():
            value = item["status"]
            if value not in stats:
                stats[value] = 0
            stats[value] += 1
        stats["total"] = sum(stats.values())
        return stats

    def results_by_status(self):
        """Return dictionary of lists of packages from comparison

        Dictionary key is the status of the comparison. Dictionary value is a
        list of tuples describing the package and nvr's.
        """
        data = {}
        for package, info in self.results.items():
            if info["status"] not in data:
                data[info["status"]] = []
            data[info["status"]].append((package, info["nvr1"], info["nvr2"]))

        return data

    def render(self, tmpl_path="templates", output_path="output", fmt="all"):
        os.makedirs(output_path, exist_ok=True)

        j2_env = jinja2.Environment(loader=jinja2.FileSystemLoader(tmpl_path))
        templates = j2_env.list_templates(extensions="j2")
        if fmt != "all":
            fmtlist = fmt.split(",")
            templates = [
                name for name in templates if name.split(".")[-2] in fmtlist
            ]

        for tmpl_name in templates:
            tmpl = j2_env.get_template(tmpl_name)
            tmpl.stream(
                source1=self.source1,
                source2=self.source2,
                product1=self.source1.product,
                product2=self.source2.product,
                results=self.results,
                stats=self.count(),
                date=datetime.datetime.now()
            ).dump(
                os.path.join(
                    output_path,
                    tmpl_name[:-3],
                )
            )

def evr(build):
    """Get epoch, version, release data from the build

    We currently reset Epoch value to 0, because we have number of cases where
    epoch of a package in Rawhide is different from that of ELN.

    We remove dist tag data from the release, so that we can compare nvr's
    between different distributions.
    """

    epoch = "0"

    version = build['version']
    p = re.compile(".(fc|eln|el)[0-9]*")
    release = re.sub(p, "", build['release'])

    return (epoch, version, release)


def compare_builds(build1, build2):
    """Compare versions of two builds

    Return -1, 0 or 1 if version of build1 is lesser, equal or greater than build2.
    """

    evr1 = evr(build1)
    evr2 = evr(build2)

    return rpm.labelCompare(evr1, evr2)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-v", "--verbose",
        help="Enable debug logging",
        action='store_true',
    )

    parser.add_argument(
        "-t", "--templates",
        default=os.path.join(sys.path[0], "templates"),
        help="Path to templates for the rendered content",
    )

    parser.add_argument(
        "-f", "--format",
        default="all",
        help="Comma-separated list of output formats. Supported: json, html, txt, all.",
    )

    parser.add_argument(
        "-o", "--output",
        default="output",
        help="Path where to store rendered results",
    )

    parser.add_argument(
        "source1",
        choices=["stream-gate", "stream-pending", "stream-candidate", "stream10-gate", "stream10-pending", "stream10-candidate"],
        help="First source of package builds",
    )

    parser.add_argument(
        "source2",
        choices=["stream-gate", "stream-pending", "stream-candidate", "stream10-gate", "stream10-pending", "stream10-candidate"],
        help="Second source of package builds",
    )

    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    source1 = BuildSource(source_id=args.source1)
    source2 = BuildSource(source_id=args.source2)

    C = Comparison(source1, source2)

    C.compare_content()

    logging.info(C.count())
    C.render(output_path=args.output, tmpl_path=args.templates, fmt=args.format)
