#!/usr/bin/bash

# Check Input
OS_VERSION="${1}"
RELEASES=(8 9 10)
if ! [[ " ${RELEASES[*]} " =~ " ${OS_VERSION} " ]] ; then
    echo ""
    echo "ERROR: ${OS_VERSION} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

# Setup Global Variables
if [ "${OS_VERSION}" == "9" ] ; then
    COMPOSE_DIR="production"
else
    COMPOSE_DIR="stream-${OS_VERSION}/production"
fi
VERSION="$(cat /mnt/centos/staged/${OS_VERSION}-stream/COMPOSE_ID | cut -d'-' -f4 )"
PROVIDERS="libvirt virtualbox"
USER="centos"
BOX="stream${OS_VERSION}"
ARCH="x86_64"
CHECKSUM_TYPE="sha256"

# Ensure we have a token
if [[ -z $VAGRANT_CLOUD_TOKEN ]] ; then
	echo "Please set VAGRANT_CLOUD_TOKEN as an environment variable" >&2
	exit 1
fi

# Create the version first
echo ; echo "== Creating ${BOX} box version: ${VERSION}" ; echo
curl \
	--request POST \
	--header "Content-Type: application/json" \
	--header "Authorization: Bearer ${VAGRANT_CLOUD_TOKEN}" \
	"https://app.vagrantup.com/api/v1/box/${USER}/${BOX}/versions" \
	--data '{ "version": { "version": "'"${VERSION}"'" } }' | jq .

for PROVIDER in ${PROVIDERS}
do
	FILENAME="CentOS-Stream-Vagrant-${OS_VERSION}-${VERSION}.${ARCH}.vagrant-${PROVIDER}.box"
	BOX_URL="https://cloud.centos.org/centos/${OS_VERSION}-stream/${ARCH}/images/${FILENAME}"
	CHECKSUM_URL="${BOX_URL}.${CHECKSUM_TYPE^^}SUM"

	CHECKSUM="$(curl --silent "$CHECKSUM_URL" | awk -F ' = ' "/\($FILENAME\)/ { print \$2 }")"
	if [[ -z $CHECKSUM ]] ; then
			echo "error: ${FILENAME} not in ${CHECKSUM_URL}" >&2
			exit 1
	fi

	# Upload the provider and  URL
	echo ; echo "== Creating ${BOX} box provider: ${PROVIDER} and URL: ${BOX_URL}" ; echo
	curl \
		--request POST \
		--header "Content-Type: application/json" \
		--header "Authorization: Bearer ${VAGRANT_CLOUD_TOKEN}" \
		"https://app.vagrantup.com/api/v1/box/${USER}/${BOX}/version/${VERSION}/providers" \
		--data '
			{
				"provider": {
					"checksum": "'"${CHECKSUM}"'",
					"checksum_type": "'"${CHECKSUM_TYPE}"'",
					"name": "'"${PROVIDER}"'",
					"url": "'"${BOX_URL}"'"
				}
			}
		' | jq .
done

# Release the version last
echo ; echo "== Releasing ${BOX} box version: ${VERSION}" ; echo
curl \
	--request PUT \
	--header "Authorization: Bearer ${VAGRANT_CLOUD_TOKEN}" \
	"https://app.vagrantup.com/api/v1/box/${USER}/${BOX}/version/${VERSION}/release" | jq .

