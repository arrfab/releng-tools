#!/usr/bin/python3
#
# mass_rebuild_file_bugs.py - A utility to discover failed builds in a
#    given tag and file bugs in bugzilla for these failed builds
#
# Copyright (C) 2013 Red Hat, Inc.
# SPDX-License-Identifier:      GPL-2.0+
#
# Authors:
#     Stanislav Ochotnicky <sochotnicky@redhat.com>
#     Mohan Boddu <mboddu@redhat.com>

from __future__ import print_function
import koji
import getpass
import tempfile
import urllib
from bugzilla.rhbugzilla import RHBugzilla
from xmlrpc.client import Fault
import operator

# contains info about all rebuilds, add new rebuilds there and update rebuildid
# here
from massrebuildsinfo import MASSREBUILDS

rebuildid = '20210809'
failures = {} # dict of owners to lists of packages that failed.
failed = [] # raw list of failed packages

bzurl = 'https://bugzilla.redhat.com'
BZCLIENT = RHBugzilla(url="%s/xmlrpc.cgi" % bzurl,
                      user="releng@fedoraproject.org")

DEFAULT_COMMENT = \
"""{component} failed to build from source in {product} {version}

https://kojihub.stream.rdu2.redhat.com//koji/taskinfo?taskID={task_id}
{extrainfo}

Please fix {component} at your earliest convenience and set the bug's status to
ASSIGNED when you start fixing it.

"""

def get_failed_builds(kojisession, epoch, buildtag, desttag, username):
    """This function returns list of all failed builds since epoch within
    buildtag that were not rebuilt succesfully in desttag

    Keyword arguments:
    kojisession -- connected koji.ClientSession instance
    epoch -- string representing date to start looking for failed builds
             from. Format: "%F %T.%N"
    buildtag -- tag where to look for failed builds (usually fXX-rebuild)
    desttag -- tag where to look for succesfully built packages
    """
    # Get the userID
    userid = kojisession.getUser(username)['id']
    # Get a list of failed build tasks since our epoch
    failtasks = sorted(kojisession.listBuilds(createdAfter=epoch, state=3, userID=userid),
                       key=operator.itemgetter('task_id'))

    # Get a list of successful builds tagged
    goodbuilds = kojisession.listTagged(buildtag, latest=True)

    # Get a list of successful builds after the epoch in our dest tag
    destbuilds = kojisession.listTagged(desttag, latest=True, inherit=True)
    for build in destbuilds:
        if build['creation_time'] > epoch:
            goodbuilds.append(build)

    pkgs = kojisession.listPackages(desttag, inherited=True)

    # get the list of packages that are blocked
    pkgs = sorted([pkg for pkg in pkgs if pkg['blocked']],
                  key=operator.itemgetter('package_id'))

    # Check if newer build exists for package
    failbuilds = []
    for build in failtasks:
        if ((not build['package_id'] in [goodbuild['package_id'] for goodbuild in goodbuilds]) and (not build['package_id'] in [pkg['package_id'] for pkg in pkgs])):
            failbuilds.append(build)

    # Generate taskinfo for each failed build
    kojisession.multicall = True
    for build in failbuilds:
        kojisession.getTaskInfo(build['task_id'], request=True)

    taskinfos = kojisession.multiCall()
    for build, [taskinfo] in zip(failbuilds, taskinfos):
        build['taskinfo'] = taskinfo
    return failbuilds

def report_failure(massrebuild, component, task_id, logs,
                   summary="{component}: FTBFS in {product} {version}",
                   comment=DEFAULT_COMMENT, extrainfo=""):
    """This function files a new bugzilla bug for component with given
    arguments

    Keyword arguments:
    massrebuild -- generic info about mass rebuild such as tracking_bug,
    Bugzilla product, version
    component -- component (package) to file bug against
    task_id -- task_id of failed build
    logs -- list of URLs to the log file to attach to the bug report
    summary -- short bug summary (if not default)
    comment -- first comment describing the bug in more detail (if not default)
    """

    format_values = dict(**massrebuild)
    format_values["task_id"] = task_id
    format_values["component"] = component
    format_values["extrainfo"] = extrainfo

    summary = summary.format(**format_values)
    comment = comment.format(**format_values)

    data = {'product': massrebuild["product"],
            'component': component,
            'version': massrebuild["version"],
            'short_desc': summary,
            'comment': comment,
            'blocks': massrebuild["tracking_bug"],
            'rep_platform': 'Unspecified',
            'bug_severity': 'unspecified',
            'op_sys': 'Unspecified',
            'bug_file_loc': '',
            'priority': 'unspecified',
           }


    try:
        print('Creating the bug report')
        bug = BZCLIENT.createbug(**data)
        bug.refresh()
        print(bug)
        attach_logs(bug, logs)
    except Fault as ex:
        print(ex)
        #Because of having image build requirement of having the image name in koji
        #as a package name, they are missing in the components of koji and we need
        #to skip them.
        #if ex.faultCode == -32000:
        #    print(component)
        if "There is no component" in ex.faultString:
            print(ex.faultString)
            return None
        elif ex.faultCode == 32000:
            print(ex.faultString)
            return None
        else:
            username = input('Bugzilla username: ')
            BZCLIENT.login(user=username,
                           password=getpass.getpass())
            return report_failure(massrebuild, component, task_id, logs, summary,
                                  comment)
    return bug

def attach_logs(bug, logs):

    if isinstance(bug, int):
        bug = BZCLIENT.getbug(bug)

    for log in logs:
        name = log.rsplit('/', 1)[-1]
        try:
            response = urllib.request.urlopen(log)
        except urllib.error.HTTPError as e:
            #sometimes there wont be any logs attached to the task.
            #skip attaching logs for those tasks
            if e.code == 404:
                print("Failed to attach {} log".format(name))
                continue
            else:
                break
        fp = tempfile.TemporaryFile()

        CHUNK = 2 ** 20
        while True:
            chunk = response.read(CHUNK)
            if not chunk:
                break
            fp.write(chunk)

        filesize = fp.tell()
        # Bugzilla file limit, still possibly too much
        # FILELIMIT = 32 * 1024
        # Just use 32 KiB:
        FILELIMIT = 2 ** 15
        if filesize > FILELIMIT:
            fp.seek(filesize - FILELIMIT)
            comment = "file {} too big, will only attach last {} bytes".format(
                name, FILELIMIT)
        else:
            comment = ""
            fp.seek(0)
        try:
            print('Attaching file %s to the ticket' % name)
            # arguments are: idlist, attachfile, description, ...
            attid = BZCLIENT.attachfile(
                bug.id, fp, name, content_type='text/plain', file_name=name,
                comment=comment
            )
        except Fault as  ex:
            print(ex)
            raise

        finally:
            fp.close()

def get_filed_bugs(tracking_bug):
    """Query bugzilla if given bug has already been filed

    arguments:
    tracking_bug -- bug used to track failures
    Keyword arguments:
    product -- bugzilla product (usually Fedora)
    component -- component (package) to file bug against
    version -- component version to file bug for (usually rawhide for Fedora)
    summary -- short bug summary
    """
    query_data = {'blocks': tracking_bug}
    bzurl = 'https://bugzilla.redhat.com'
    bzclient = RHBugzilla(url="%s/xmlrpc.cgi" % bzurl)

    return bzclient.query(query_data)

def get_task_failed(kojisession, task_id):
    ''' For a given task_id, use the provided kojisession to return the
    task_id of the first children that failed to build.
    '''
    for child in kojisession.getTaskChildren(task_id):
        if child['state'] == koji.TASK_STATES["FAILED"]:  # 5 == Failed
            return child['id']


if __name__ == '__main__':
    massrebuild = MASSREBUILDS[rebuildid]

    kojisession = koji.ClientSession('https://kojihub.stream.rdu2.redhat.com/kojihub')
    print('Getting the list of failed builds...')
    failbuilds = get_failed_builds(kojisession, massrebuild['epoch_ftbfs'],
                                   massrebuild['buildtag'],
                                   massrebuild['desttag'],
                                   massrebuild['centpkg_user'])
    print('Getting the list of filed bugs...')
    filed_bugs = get_filed_bugs(massrebuild['tracking_bug'])
    filed_bugs_components = [bug.component for bug in filed_bugs]
    for build in failbuilds:
        task_id = build['task_id']
        component = build['package_name']
        work_url = 'https://kojihub.stream.rdu2.redhat.com/kojifiles/work'

        child_id = get_task_failed(kojisession, task_id)
        if not child_id:
            print('No children failed for task: %s (%s)' % (
                task_id, component))
            logs = []
        else:
            base_path = koji.pathinfo.taskrelpath(child_id)
            log_url = "%s/%s/" % (work_url, base_path)
            build_log = log_url + "build.log"
            root_log = log_url + "root.log"
            state_log = log_url + "state.log"
            logs = [build_log, root_log, state_log]


        if component not in filed_bugs_components:
            print("Filing bug for %s" % component)
            report_failure(massrebuild, component, task_id, logs)
            filed_bugs_components.append(component)
        else:
            print("Skipping %s, bug already filed" % component)
