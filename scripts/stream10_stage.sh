#!/usr/bin/bash

set -euo pipefail
IFS=$'\n\t'

if [[ $USER != 'compose' ]]; then
    echo "This script must be run as the compose user."
    exit 1
fi

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="10-stream"

DRY_RUN=$1

function message() {
    echo -e "\e[36m* $1\e[m"
}


function sync_rpms() {
    arch=$1
    shift
    variants=($@)
    for variant in ${variants[@]}; do
        VARIANT_DIR=/mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/
        if ! [ -d ${VARIANT_DIR} ] ; then
            echo "ERROR: ${VARIANT_DIR} NOT FOUND"
            echo "  POSSIBLE THAT NETWORK DISK IS NOT MOUNTED"
            exit 1
        else
            if [ $DRY_RUN ]; then
                rsync -avhH --info progress2 --link-dest=/mnt/centos --include="os/images" --exclude="images/" --exclude="iso/" --dry-run \
                    /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/$arch/ /mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/
            else
                message "syncing $variant $arch"
                # delete old comps files first to ensure we use the new ones
                rm -vf ${VARIANT_DIR}/os/repodata/*-comps-*.xml*
                # rsync repo from compose
                rsync -avhH --info progress2 --link-dest=/mnt/centos --include="os/images" --exclude="images/" --exclude="iso/" \
                    /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/$arch/ ${VARIANT_DIR}/
                # modular metadata must be decompressed for createrepo_c to pick it up
                # https://github.com/rpm-software-management/createrepo_c/issues/263
                pushd /mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/os
                # update repodata, make sure modules work
                find repodata -type f -name '*-modules.yaml.gz' -exec gunzip -vfk {} \;
                find repodata -type f -name '*-modules.yaml.xz' -exec unxz -vfk {} \;
                createrepo_c --update --workers 8 --xz \
                    --distro 'cpe:/o:centos-stream:centos-stream:10,CentOS Stream 10' \
                    --revision 10-stream \
                    --groupfile $(find repodata -type f -name '*-comps-*.xml') \
                    --retain-old-md-by-age 1m \
                    .
                # Use updated repodata to clean out old rpms, but keep the newest 5
                dnf --quiet repomanage --old --keep 5 . 2>/dev/null | while read pkg ; do
                echo "Removing old package: ${pkg}"
                rm -f ${pkg}
                done
                # update the repodata again
                find repodata -type f -name '*-modules.yaml.gz' -exec gunzip -vfk {} \;
                find repodata -type f -name '*-modules.yaml.xz' -exec unxz -vfk {} \;
                createrepo_c --update --workers 8 --xz \
                    --distro 'cpe:/o:centos-stream:centos-stream:10,CentOS Stream 10' \
                    --revision 10-stream \
                    --groupfile $(find repodata -type f -name '*-comps-*.xml') \
                    --retain-old-md-by-age 1m \
                    .
                popd
            fi
        fi
    done
}


function sync_source_rpms() {
    variants=($@)
    for variant in ${variants[@]}; do
        if ! [ -d /mnt/centos/staged/$CENTOS_RELEASE/$variant/source/tree/ ] ; then
            mkdir -p /mnt/centos/staged/$CENTOS_RELEASE/$variant/source/tree/
        fi
        pushd /mnt/centos/staged/$CENTOS_RELEASE/$variant/source/tree/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --link-dest=/mnt/centos --dry-run \
                /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/source/tree/ ./
        else
            message "syncing source $variant"
            rsync -avhH --info progress2 --link-dest=/mnt/centos \
                /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/source/tree/ ./
            createrepo_c --update --workers 8 --xz .
            # Use updated repodata to clean out old rpms, but keep the newest 5
            dnf --quiet repomanage --old --keep 5 . 2>/dev/null | while read pkg ; do
              echo "Removing old package: ${pkg}"
              rm -f ${pkg}
            done
            createrepo_c --update --workers 8 --xz .
        fi
        popd
    done
}

pushd ${HOME} # We're running as the compose user so we want to be in a CWD that we can go back and forth to

sync_rpms aarch64 BaseOS AppStream CRB HighAvailability 
sync_rpms ppc64le BaseOS AppStream CRB HighAvailability
sync_rpms s390x BaseOS AppStream CRB HighAvailability
sync_rpms x86_64 BaseOS AppStream CRB HighAvailability NFV RT

jq -r '.payload.compose.id' \
    /mnt/centos/composes/stream-10/production/$COMPOSE/compose/metadata/composeinfo.json \
    > /mnt/centos/staged/$CENTOS_RELEASE/COMPOSE_ID

sync_source_rpms BaseOS AppStream CRB HighAvailability NFV RT
