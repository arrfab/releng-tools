#!/usr/bin/bash

set -euo pipefail
IFS=$'\n\t'

if [[ $USER != 'compose' ]]; then
    echo "This script must be run as the compose user."
    exit 1
fi

COMPOSE="latest-CentOS-Stream"
CENTOS_RELEASE="10-stream"

DRY_RUN=$1

function message() {
    echo -e "\e[36m* $1\e[m"
}


function sync_isos() {
    arch=$1
    shift
    variants=($@)
    for variant in ${variants[@]}; do
        VARIANT_DIR=/mnt/centos/staged/$CENTOS_RELEASE/$variant/$arch/iso/
        if [ $DRY_RUN ]; then
            rsync -avhH --info progress2 --dry-run --delete \
                /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/$arch/iso/ $VARIANT_DIR
        else
            message "syncing $arch isos"
            # rsync iso directory from compose
            rsync -avhH --info progress2 --delete \
               /mnt/centos/composes/stream-10/production/$COMPOSE/compose/$variant/$arch/iso/ $VARIANT_DIR
            # set up latest symlinks
            pushd $VARIANT_DIR
            for f in *.iso; do
                datenum="$(echo $f |cut -d'-' -f4)"
                ln -sf $f $(echo $f | sed "s/$datenum/latest/");
            done

            # Latest checksum files should be real files not symlinks
            for f in *.iso.*SUM; do
                datenum="$(echo $f |cut -d'-' -f4)"
                cp -f $f $(echo $f | sed "s/$datenum/latest/");
            done

            # Rewrite the filenames in the contents of the latest checksum files
            for f in *-latest*.iso.*SUM; do
                datenum="$(head -1 $f |cut -d'-' -f4)"
                sed "s/$datenum/latest/" -i ${f}
            done

            popd
        fi
    done
}

pushd ${HOME} # We're running as the compose user so we want to be in a CWD that we can go back and forth to

sync_isos aarch64 BaseOS
sync_isos ppc64le BaseOS
sync_isos s390x BaseOS
sync_isos x86_64 BaseOS
